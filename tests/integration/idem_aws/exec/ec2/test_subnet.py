import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_ec2_subnet):
    subnet_get_name = "idem-test-exec-get-subnet-" + str(int(time.time()))
    subnet_fixture = hub.tool.aws.ec2.conversion_utils.convert_raw_subnet_to_present(
        aws_ec2_subnet, "idem-fixture-subnet"
    )
    tags = {"Name": subnet_fixture["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.subnet.get(
        ctx,
        name=subnet_get_name,
        default_for_az="false",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert subnet_get_name == resource.get("name")
    assert subnet_fixture["cidr_block"] == resource.get("cidr_block")
    assert subnet_fixture["vpc_id"] == resource.get("vpc_id")
    assert subnet_fixture["resource_id"] == resource.get("resource_id")
    assert tags == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get_invalid_resource_id(hub, ctx):
    subnet_get_name = "idem-test-exec-get-subnet-" + str(int(time.time()))
    ret = await hub.exec.aws.ec2.subnet.get(
        ctx,
        name=subnet_get_name,
        resource_id="fake-id",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.ec2.subnet '{subnet_get_name}' result is empty" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_ec2_subnet):
    subnet_get_name = "idem-test-exec-get-subnet-" + str(int(time.time()))
    subnet_fixture = hub.tool.aws.ec2.conversion_utils.convert_raw_subnet_to_present(
        aws_ec2_subnet, "idem-fixture-subnet"
    )
    tags = {"Name": subnet_fixture["tags"]["Name"]}
    ret = await hub.exec.aws.ec2.subnet.list(
        ctx,
        name=subnet_get_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert subnet_fixture["cidr_block"] == resource.get("cidr_block")
    assert subnet_fixture["vpc_id"] == resource.get("vpc_id")
    assert subnet_fixture["resource_id"] == resource.get("resource_id")
    assert tags == resource.get("tags")

    tags = {"Name": subnet_fixture["tags"]["Name"], "fake-key": "fake-value"}
    ret = await hub.exec.aws.ec2.subnet.list(
        ctx,
        name=subnet_get_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 0
