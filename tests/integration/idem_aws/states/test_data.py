async def test_lookup(hub, ctx, aws_ec2_vpc):
    vpc_id = aws_ec2_vpc["VpcId"]
    ret = await hub.states.aws.data.lookup(
        ctx,
        name="lookup a VPC",
        service="ec2",
        resource="vpc",
        filter=f"Vpcs[?VpcId=='{vpc_id}']",
    )
    assert ret["result"] is True, ret["comment"]
    assert ret["new_state"] == aws_ec2_vpc
