import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_policy_attachment(
    hub,
    ctx,
    aws_organization_unit,
    aws_organization_policy,
):
    attach_policy_organization = "idem-test-policy-organization-attachment-" + str(
        uuid.uuid4()
    )

    policy_id = aws_organization_policy["resource_id"]

    # attach policy to the target
    ret = await hub.states.aws.organizations.policy_attachment.present(
        ctx,
        name=attach_policy_organization,
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_id == resource.get("PolicyId")

    # describe policy attachments to organization root, account or OU
    describe_ret = await hub.states.aws.organizations.policy_attachment.describe(ctx)

    assert f"{aws_organization_unit['resource_id']}-{policy_id}" in describe_ret
    policy_attachment = describe_ret.get(
        f"{aws_organization_unit['resource_id']}-{policy_id}"
    ).get("aws.organizations.policy_attachment.present")
    assert policy_id == policy_attachment[0].get("policy_id")
    assert aws_organization_unit["resource_id"] == policy_attachment[1].get("target_id")

    # Detach the organization SCP policy from OU
    ret = await hub.states.aws.organizations.policy_attachment.absent(
        ctx,
        name="test",
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    ret = await hub.states.aws.organizations.policy_attachment.absent(
        ctx,
        name="test",
        policy_id=policy_id,
        target_id=aws_organization_unit["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
