import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_organization_policy(hub, ctx, aws_organization):
    policy_name = "idem-test-organization-policy-" + str(uuid.uuid4())
    description = "Enables admins of attached accounts to delegate all S3 permissions"
    policy_type = "SERVICE_CONTROL_POLICY"
    content = """{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:DeleteBucket",
        "s3:DeleteObject",
        "s3:DeleteObjectVersion"
      ],
      "Resource": "*",
      "Effect": "Deny"
    }
  ]
}"""

    create_tag = {"org": "idem", "env": "test"}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create Organization policy in test context
    create_policy_ret = await hub.states.aws.organizations.policy.present(
        test_ctx,
        name=policy_name,
        description=description,
        policy_type=policy_type,
        content=content,
        tags=create_tag,
    )

    assert create_policy_ret["result"], create_policy_ret["comment"]
    assert (
        f"Would create aws.organizations.policy '{policy_name}'"
        in create_policy_ret["comment"]
    )
    assert not create_policy_ret.get("old_state")
    assert create_policy_ret.get("new_state")

    if not hub.tool.utils.is_running_localstack(ctx):
        assert create_tag == create_policy_ret.get("new_state").get(
            "tags"
        ), "create_tag should equal the tag in new_state"

    assert description == create_policy_ret.get("new_state").get(
        "description"
    ), "description should equal to the description in new_state"

    assert policy_name == create_policy_ret.get("new_state").get(
        "name"
    ), "name should equal the name in new_state"

    assert policy_type == create_policy_ret.get("new_state").get(
        "policy_type"
    ), "policy_type should equal to the Type in new_state"

    assert content == create_policy_ret.get("new_state").get(
        "content"
    ), "content should equal the Content in new_state"

    # Create in real
    create_policy_ret = await hub.states.aws.organizations.policy.present(
        ctx,
        name=policy_name,
        description=description,
        policy_type=policy_type,
        content=content,
        tags=create_tag,
    )

    assert create_policy_ret
    if not hub.tool.utils.is_running_localstack(ctx):
        assert create_policy_ret["result"], create_policy_ret["comment"]
    assert not create_policy_ret.get("old_state")
    assert create_policy_ret.get("new_state")
    assert (
        f"Created aws.organizations.policy '{policy_name}'."
        in create_policy_ret["comment"]
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert create_tag == create_policy_ret.get("new_state").get(
            "tags"
        ), "create_tag should equal the tag in new_state"

    assert description == create_policy_ret.get("new_state").get(
        "description"
    ), "description should equal to the description in new_state"

    assert policy_name == create_policy_ret.get("new_state").get(
        "name"
    ), "name should equal the name in new_state"

    assert policy_type == create_policy_ret.get("new_state").get(
        "policy_type"
    ), "policy_type should equal to the Type in new_state"

    assert content == create_policy_ret.get("new_state").get(
        "content"
    ), "content should equal the Content in new_state"

    updated_description = "idem_test_aws_update_ " + description
    update_tags = {"org": "update_idem", "state": "temp"}

    # Update in test
    update_policy_ret = await hub.states.aws.organizations.policy.present(
        test_ctx,
        name=policy_name,
        resource_id=create_policy_ret.get("new_state")["resource_id"],
        description=updated_description,
        content=content,
        tags=update_tags,
    )

    assert update_policy_ret
    if not hub.tool.utils.is_running_localstack(ctx):
        assert update_policy_ret["result"], update_policy_ret["comment"]
    assert (
        f"Would update aws.organizations.policy '{policy_name}'"
        in update_policy_ret["comment"]
    )

    assert update_policy_ret.get("old_state")
    assert update_policy_ret.get("old_state").get(
        "resource_id"
    ) == create_policy_ret.get("new_state").get(
        "resource_id"
    ), "Resource_id will remain the same as its the case of update"

    assert update_policy_ret.get("old_state").get("arn") == create_policy_ret.get(
        "new_state"
    ).get("arn"), "New State of create arn should be equal to Old state of update arn"

    assert update_policy_ret.get("old_state").get("name") == create_policy_ret.get(
        "new_state"
    ).get(
        "name"
    ), "New State of create Name should be equal to Old state of update Name"

    assert update_policy_ret.get("old_state").get(
        "description"
    ) == create_policy_ret.get("new_state").get(
        "description"
    ), "New State of create Description should be equal to Old state of update Description"

    assert update_policy_ret.get("old_state").get(
        "policy_type"
    ) == create_policy_ret.get("new_state").get(
        "policy_type"
    ), "New State of create Type should be equal to Old state of update Type"

    assert update_policy_ret.get("old_state").get(
        "aws_managed"
    ) == create_policy_ret.get("new_state").get(
        "aws_managed"
    ), "New State of create aws_managed should be equal to Old state of update aws_managed"

    assert update_policy_ret.get("old_state").get("content") == create_policy_ret.get(
        "new_state"
    ).get(
        "content"
    ), "New State of create Content should be equal to Old state of update Content"

    if "tags" in update_policy_ret.get("old_state"):
        assert update_policy_ret.get("old_state").get("tags") == create_policy_ret.get(
            "new_state"
        ).get(
            "tags"
        ), "New State of create Tags should be equal to Old state of update Tags"

    assert updated_description == update_policy_ret.get("new_state").get(
        "description"
    ), "description should equal to the updated description in new_state"

    assert policy_name == update_policy_ret.get("new_state").get(
        "name"
    ), "Policy Name should not change as there was no update to policy name"

    if not hub.tool.utils.is_running_localstack(ctx):
        assert update_policy_ret.get("new_state").get("tags") == update_tags

    # Update in real
    update_policy_ret = await hub.states.aws.organizations.policy.present(
        ctx,
        name=policy_name,
        resource_id=create_policy_ret.get("new_state")["resource_id"],
        description=updated_description,
        content=content,
        tags=update_tags,
    )

    assert update_policy_ret
    if not hub.tool.utils.is_running_localstack(ctx):
        assert update_policy_ret["result"], update_policy_ret["comment"]
    assert (
        f"Updated aws.organizations.policy '{policy_name}'"
        in update_policy_ret["comment"]
    )
    assert update_policy_ret.get("old_state")

    assert update_policy_ret.get("old_state").get(
        "resource_id"
    ) == create_policy_ret.get("new_state").get(
        "resource_id"
    ), "Resource_id will remain the same as its the case of update"

    assert update_policy_ret.get("old_state").get("arn") == create_policy_ret.get(
        "new_state"
    ).get("arn"), "New State of create arn should be equal to Old state of update arn"

    assert update_policy_ret.get("old_state").get("name") == create_policy_ret.get(
        "new_state"
    ).get(
        "name"
    ), "New State of create Name should be equal to Old state of update Name"

    assert update_policy_ret.get("old_state").get(
        "description"
    ) == create_policy_ret.get("new_state").get(
        "description"
    ), "New State of create Description should be equal to Old state of update Description"

    assert update_policy_ret.get("old_state").get(
        "policy_type"
    ) == create_policy_ret.get("new_state").get(
        "policy_type"
    ), "New State of create Type should be equal to Old state of update Type"

    assert update_policy_ret.get("old_state").get(
        "aws_managed"
    ) == create_policy_ret.get("new_state").get(
        "aws_managed"
    ), "New State of create aws_managed should be equal to Old state of update aws_managed"

    assert update_policy_ret.get("old_state").get("content") == create_policy_ret.get(
        "new_state"
    ).get(
        "content"
    ), "New State of create Content should be equal to Old state of update Content"

    if "tags" in update_policy_ret.get("old_state"):
        assert update_policy_ret.get("old_state").get("tags") == create_policy_ret.get(
            "new_state"
        ).get(
            "tags"
        ), "New State of create Tags should be equal to Old state of update Tags"

    assert updated_description == update_policy_ret.get("new_state").get(
        "description"
    ), "description should equal to the updated description in new_state"

    assert policy_name == update_policy_ret.get("new_state").get(
        "name"
    ), "Policy Name should not change as there was no update to policy name"

    if not hub.tool.utils.is_running_localstack(ctx):
        assert update_policy_ret.get("new_state").get("tags") == update_tags

    describe_ret = await hub.states.aws.organizations.policy.describe(ctx)
    describe_params = describe_ret[update_policy_ret.get("new_state")["name"]].get(
        "aws.organizations.policy.present"
    )

    described_resource_map = dict(ChainMap(*describe_params))
    assert update_policy_ret.get("new_state")["name"] == described_resource_map["name"]
    assert (
        update_policy_ret.get("new_state")["description"]
        == described_resource_map["description"]
    )
    assert (
        update_policy_ret.get("new_state")["policy_type"]
        == described_resource_map["policy_type"]
    )
    assert (
        update_policy_ret.get("new_state")["content"]
        == described_resource_map["content"]
    )
    assert (
        update_policy_ret.get("new_state")["resource_id"]
        == described_resource_map["resource_id"]
    )
    assert update_policy_ret.get("new_state")["arn"] == described_resource_map["arn"]
    assert (
        update_policy_ret.get("new_state")["aws_managed"]
        == described_resource_map["aws_managed"]
    )

    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            update_policy_ret.get("new_state")["tags"] == described_resource_map["tags"]
        )

    # Delete organizations.policy with test flag
    ret = await hub.states.aws.organizations.policy.absent(
        test_ctx,
        name=update_policy_ret.get("new_state").get("name"),
        resource_id=update_policy_ret.get("new_state").get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Would delete aws.organizations.policy '{policy_name}'" in ret["comment"]

    # Delete in real
    delete_policy_ret = await hub.states.aws.organizations.policy.absent(
        ctx,
        name=update_policy_ret.get("new_state").get("name"),
        resource_id=update_policy_ret.get("new_state").get("resource_id"),
    )

    assert delete_policy_ret["result"], delete_policy_ret["comment"]

    assert (
        f"aws.organizations.policy {update_policy_ret.get('new_state').get('name')} deleted."
        in delete_policy_ret["comment"]
    )

    delete_policy_ret_again = await hub.states.aws.organizations.policy.absent(
        ctx,
        name=update_policy_ret.get("new_state").get("name"),
        resource_id=update_policy_ret.get("new_state").get("resource_id"),
    )

    assert (
        f"aws.organizations.policy '{update_policy_ret.get('new_state').get('name')}' already absent"
        in delete_policy_ret_again["comment"]
    )
    assert not delete_policy_ret_again.get(
        "old_state"
    ) and not delete_policy_ret_again.get("new_state")
