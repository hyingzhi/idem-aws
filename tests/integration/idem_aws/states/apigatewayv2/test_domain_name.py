import asyncio
import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_certificate_manager_import, cleanup):
    global PARAMETER
    ctx["test"] = __test
    domain_name = aws_certificate_manager_import["domain_name"]
    domain_arn = aws_certificate_manager_import["resource_id"]
    PARAMETER["name"] = domain_name
    endpoint_type = "REGIONAL"
    security_policy = "TLS_1_2"
    PARAMETER["domain_name_configurations"] = [
        {
            "CertificateArn": domain_arn,
            "CertificateName": domain_name,
            "EndpointType": endpoint_type,
            "SecurityPolicy": security_policy,
        }
    ]
    PARAMETER["tags"] = {
        "idem-resource-name": domain_name,
        "time": str(int(time.time())),
    }

    ret = await hub.states.aws.apigatewayv2.domain_name.present(ctx, **PARAMETER)
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.apigatewayv2.domain_name '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            f"Created aws.apigatewayv2.domain_name '{PARAMETER['name']}'"
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert PARAMETER["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert PARAMETER["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigatewayv2.domain_name.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that the describe output format is correct
    assert "aws.apigatewayv2.domain_name.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id][
        "aws.apigatewayv2.domain_name.present"
    ]
    described_resource_map = dict(ChainMap(*described_resource))
    assert 1 == len(described_resource_map.get("domain_name_configurations"))
    domain_name_configuration = described_resource_map["domain_name_configurations"][0]
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert PARAMETER["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert PARAMETER["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    assert PARAMETER["tags"] == described_resource_map.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create an domain_name fixture for exec.get() testing
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigatewayv2.domain_name.get(
        ctx=ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert 1 == len(resource.get("domain_name_configurations"))
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert PARAMETER["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert PARAMETER["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_domain_name", depends=["describe"])
async def test_update_domain_name(hub, ctx, __test):
    # this is to avoid a "TooManyRequestsException" thrown by the API Gateway servers
    # (request is being made too frequently and is more than what the server can handle)
    if not hub.tool.utils.is_running_localstack(ctx):
        await asyncio.sleep(30)
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["domain_name_configurations"][0]["CertificateName"] = (
        "new." + PARAMETER["name"]
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        new_parameter["tags"] = {
            "idem-resource-new-name": PARAMETER["name"],
            "time": str(int(time.time())),
        }
    ret = await hub.states.aws.apigatewayv2.domain_name.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    domain_name_configuration = old_resource.get("domain_name_configurations")[0]
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert PARAMETER["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert PARAMETER["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    assert PARAMETER["tags"] == old_resource.get("tags")
    resource = ret["new_state"]
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["resource_id"] == resource.get("resource_id")
    domain_name_configuration = resource.get("domain_name_configurations")[0]
    assert new_parameter["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert new_parameter["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert new_parameter["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert new_parameter["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    assert new_parameter["tags"] == resource.get("tags")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_domain_name"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    domain_name_configuration = old_resource.get("domain_name_configurations")[0]
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateArn"
    ] == domain_name_configuration.get("CertificateArn")
    assert PARAMETER["domain_name_configurations"][0][
        "CertificateName"
    ] == domain_name_configuration.get("CertificateName")
    assert PARAMETER["domain_name_configurations"][0][
        "EndpointType"
    ] == domain_name_configuration.get("EndpointType")
    assert PARAMETER["domain_name_configurations"][0][
        "SecurityPolicy"
    ] == domain_name_configuration.get("SecurityPolicy")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigatewayv2.domain_name", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigatewayv2.domain_name", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.domain_name", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(depends=["present"])
async def test_absent_with_none_resource_id(hub, ctx):
    # Delete apigatewayv2 domain_name with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.domain_name.absent(
        ctx, name=PARAMETER["name"], resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.domain_name", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigatewayv2.domain_name.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]
