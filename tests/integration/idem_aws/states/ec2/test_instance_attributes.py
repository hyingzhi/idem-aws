"""
Tests using the instance fixture to modify attributes
"""
import pytest


def setup_module():
    raise pytest.skip("Day2 operations are not yet ready")


@pytest.mark.localstack(False)
def test_bootstrap_salt(aws_ec2_instance, __test):
    """
    Verify the ability to bootstrap an instance with salt and then remove salt
    """
